using System.Data.Entity;
using TDD.Model;

namespace TDD.DAL.EF
{
    public class ProductsAppContext : DbContext
    {
        public ProductsAppContext()
        {}

        //public ProductsAppContext(string name) : base(name)
        //{}

        public DbSet<Product> Products { get; set; }
    }
}