using System.Web.Mvc;
using Microsoft.Practices.Unity;
using TDD.DAL.EF;
using TDD.Model;
using TDD.Services;
using Unity.Mvc4;

namespace TDD.WebUi.Mvc
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);
      container.RegisterType<IProductService, ProductService>();
      container.RegisterType<IProductRepository, ProductsRepositoryEf>();
      container.RegisterType<ProductsRepositoryEf>();
    
      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
    
    }
  }
}