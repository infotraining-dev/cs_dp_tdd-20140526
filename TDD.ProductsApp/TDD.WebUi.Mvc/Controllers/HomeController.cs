﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TDD.Services;

namespace TDD.WebUi.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductService _productService;

        public HomeController(IProductService productService)
        {
            _productService = productService;
        }

        //
        // GET: /Home/

        public ActionResult Index()
        {
            ViewBag.Title = "Wishlist";

            var response = _productService.GetAllProducts();

            return View(response.Products);
        }

    }
}
