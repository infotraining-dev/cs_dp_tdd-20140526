﻿using System.Collections.Generic;

namespace TDD.Model
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();

        Product FindByName(string p);

        void Add(Product product);

        void SaveChanges();
    }
}
