﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TDD.Model;
using TDD.Services.RequestResponse;

namespace TDD.Services_Tests
{
    [TestFixture]
    [Category("ProductService")]
    public class When_listing_products : With_ProductService
    {
        private List<Product> _products;

        [SetUp]
        public void Init()
        {
            _products = new List<Product>()
            {
                new Product() {Id = 1, Name = "P3"},
                new Product() {Id = 2, Name = "P2"},
                new Product() {Id = 3, Name = "P1"}
            };
        }
        
        [Test]
        public void ShouldReturnProductsInAlphabeticalOrder()
        {
            _mqProductRepository.Setup(m => m.GetAll()).Returns(_products);

            var expectedOrder = _products.OrderBy(p => p.Name);

            GetAllProductsResponse response = _productService.GetAllProducts();

            CollectionAssert.AreEqual(expectedOrder, response.Products);
        }

        [Test]
        public void ShouldReturnAllProducts()
        {
            _mqProductRepository.Setup(m => m.GetAll()).Returns(_products);

            var response = _productService.GetAllProducts();

            Assert.That(response.Products.Count(), Is.EqualTo(_products.Count));
        }

        [Test]
        public void WhenExceptionFromRepositoryShouldReturnErrorMessage()
        {
            _mqProductRepository.Setup(m => m.GetAll()).Throws(new Exception("Db Error"));

            var response = _productService.GetAllProducts();

            Assert.That(response.Success, Is.False);
            Assert.That(response.Message, Is.EqualTo("Sorry! Service doesn't work"));
        }

        [Test]
        public void WhenRepositoryWorksShouldSetSuccessOnTrue()
        {
            _mqProductRepository.Setup(m => m.GetAll()).Returns(_products);

            var response = _productService.GetAllProducts();

            Assert.That(response.Success, Is.True);
        }
    }
}
