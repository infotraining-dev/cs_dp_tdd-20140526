﻿using System.Collections.Generic;
using TDD.Model;

namespace TDD.Services.RequestResponse
{
    public class GetAllProductsResponse : ResponseBase
    {
        public IEnumerable<Product> Products { get; set; }
    }
}
