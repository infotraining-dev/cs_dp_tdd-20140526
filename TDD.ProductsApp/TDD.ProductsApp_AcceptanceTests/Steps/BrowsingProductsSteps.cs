﻿using System;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WatiN.Core;
using Table = TechTalk.SpecFlow.Table;

namespace TDD.ProductsApp_AcceptanceTests.Steps
{

    public class WebBrowser
    {
        public static IE Current
        {
            get
            {
                if (!ScenarioContext.Current.ContainsKey("browser"))
                    ScenarioContext.Current["browser"] = new IE();

                return ScenarioContext.Current["browser"] as IE;
            }
        }
    }

    [Binding]
    public class ScenarioSetupTeardown
    {
        [BeforeScenario]
        public void Init()
        {
        }

        [AfterScenario]
        public void Teardown()
        {
            WebBrowser.Current.Close();
            WebBrowser.Current.Dispose();
        }
    }


    [Binding]
    public class BrowsingProductsSteps
    {
        

        [When]
        public void When_I_navigate_to_Home_page()
        {
            var ie = WebBrowser.Current;
            ie.GoTo("http://localhost:8090/");
            ie.BringToFront();
        }

        [Then]
        public void Then_the_title_of_the_page_TITLE_is_displayed(string title)
        {
            Assert.True(WebBrowser.Current.Title.Contains(title));
            Assert.True(WebBrowser.Current.ContainsText(title));
        }
        
        [Then]
        public void Then_the_list_of_products_in_alphabetical_order_is_displayed(Table table)
        {
            var expectedProducts = table.Rows.Select(r => r[1]).ToList();

            var htmlTable = WebBrowser.Current.Table("products-list");

            var actualProducts = htmlTable.TableRows
                .Select(tr => tr.TableCells[1].Text).ToList();

            CollectionAssert.AreEqual(expectedProducts, actualProducts);
        }
    }
}
