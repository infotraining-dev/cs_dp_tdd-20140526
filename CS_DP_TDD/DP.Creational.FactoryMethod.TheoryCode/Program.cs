﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.FactoryMethod.TheoryCode
{
    /// <summary>
    /// Factory Method Design Pattern.
    /// </summary>
    class MainApp
    {
        static void Main()
        {
            Creator c = new ConcreteCreatorA();

            Client client = new Client(c);
            client.DoSomething();

            Console.WriteLine();

            client.Creator = new ConcreteCreatorB();
            client.DoSomething();

            Console.WriteLine();

            client.Creator = new ConcreteCreatorC();
            client.DoSomething();
        }
    }

    // "IProduct" 
    interface IProduct
    {
    }

    // "ConcreteProduct" 
    class ConcreteProductA : IProduct
    {
    }

    // "ConcreteProduct" 
    class ConcreteProductB : IProduct
    {
    }

    class ConcreteProductC : IProduct
    {
        
    }

    // "Creator"
    abstract class Creator
    {
        public abstract IProduct CreateProduct();
    }

    // "ConcreteCreator"
    class ConcreteCreatorA : Creator
    {
        public override IProduct CreateProduct()
        {
            return new ConcreteProductA();
        }
    }

    // "ConcreteCreator"
    class ConcreteCreatorB : Creator
    {
        public override IProduct CreateProduct()
        {
            return new ConcreteProductB();
        }
    }

    class ConcreteCreatorC : Creator
    {
        public override IProduct CreateProduct()
        {
            return new ConcreteProductC();
        }
    }


    // "Client"
    class Client
    {
        private Creator creator;
        public Creator Creator
        {
            set
            {
                creator = value;
                FillProducts();
            }
        }

        List<IProduct> products = new List<IProduct>();

        public Client(Creator creator)
        {
            Creator = creator;
        }

        private void FillProducts()
        {
            products.Clear();

            for (int i = 0; i < 10; ++i)
                products.Add(creator.CreateProduct());
        }

        public void DoSomething()
        {
            Console.WriteLine("Client operates on:");
            foreach(var p in products)
                Console.WriteLine(" + {0}", p.GetType().Name);
        }
    }
}
