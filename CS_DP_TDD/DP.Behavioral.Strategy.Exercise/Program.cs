﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DP.Behavioral.Strategy.Exercise
{
    public struct Result
    {
        public string Description { get; set; }
        public double Value { get; set; }

        public Result(string desc, double val) : this()
        {
            Description = desc;
            Value = val;
        }
    }

    public enum StatType
    {
        Avg, Sum, Mediana
    }

    public interface IStatistics
    {
        void Calculate(IList<double> data, List<Result> results);
    }

    public class Avg : IStatistics
    {
        private const string StatId = "AVG";

        public void Calculate(IList<double> data, List<Result> results)
        {
            double avg = data.Average();
            results.Add(new Result(StatId, avg));
        }
    }

    public class Sum : IStatistics
    {
        private const string StatId = "SUM";

        public void Calculate(IList<double> data, List<Result> results)
        {
            double sum = data.Sum();

            results.Add(new Result(StatId, sum));   
        }
    }

    public class Mediana : IStatistics
    {
        private const string StatId = "MEDIANA";

        public void Calculate(IList<double> data, List<Result> results)
        {
            double mediana;

            double[] values = data.OrderBy(v => v).ToArray();
            values.OrderBy(v => v);

            int length = values.Length;

            if (length % 2 != 0)
                mediana = values[(length + 1) / 2];
            else
                mediana = (values[length / 2] + values[length / 2 + 1]) / 2;

            results.Add(new Result(StatId, mediana));
        }
    }

    public class StatGroup : IStatistics
    {
        private List<IStatistics> _stats = new List<IStatistics>();

        public void Add(IStatistics stat)
        {
            _stats.Add(stat);
        }

        public void Calculate(IList<double> data, List<Result> results)
        {
            foreach (var s in _stats)
            {
                s.Calculate(data, results);
            }
        }
    }

    public class DataAnalyzer
    {
        public IStatistics Statistics { get; set; }

        double[] Data { get; set; }
        List<Result> results = new List<Result>();

        public DataAnalyzer(IStatistics statistics)
        {
            Statistics = statistics;
        }

        public virtual void LoadData(string path)
        {
            List<double> values = new List<double>();

            using (StreamReader reader = File.OpenText(path))
            {
                string line = reader.ReadToEnd();

                foreach (string token in line.Split(' '))
                    values.Add(double.Parse(token));
            }

            Data = values.ToArray();

            results.Clear();
        }

        public void CalcStats()
        {
            Statistics.Calculate(Data, results);
        }

        public List<Result> Results
        {
            get { return results; }   
        }   
    }

    class Program
    {
        public static void ShowResults(List<Result> results)
        {
            Console.WriteLine("Statistics for data:");
            foreach (var result in results)
                Console.WriteLine(" + {0} = {1}", result.Description, result.Value);
        }

        public static void Main()
        {
            StatGroup avgGroup = new StatGroup();
            avgGroup.Add(new Avg());
            avgGroup.Add(new Mediana());

            StatGroup stdGroup = new StatGroup();
            stdGroup.Add(avgGroup);
            stdGroup.Add(new Sum());

            DataAnalyzer dataAnalyzer = new DataAnalyzer(stdGroup);
            dataAnalyzer.LoadData("test.dat");
            dataAnalyzer.CalcStats();
            
            ShowResults(dataAnalyzer.Results);

        }
    }
}
