﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoqMocks
{

    public interface ILongRunningLibrary
    {
        string RunForALongTime(int interval);

        void Run(string arg1, int arg2);
    }

    public class LongRunningLibrary : ILongRunningLibrary
    {
        public string RunForALongTime(int interval)
        {
            var timeToWait = interval*1000;

            Thread.Sleep(timeToWait);

            return string.Format("Waited {0} sec.", interval);
        }


        public void Run(string arg1, int arg2)
        {
            Random rand = new Random();
            var timeToWait = rand.Next(10) * 1000;

            Thread.Sleep(timeToWait);
        }
    }

    public class DependentObject
    {
        private ILongRunningLibrary _library;

        public DependentObject(ILongRunningLibrary library)
        {
            _library = library;
        }

        public void DoStuff()
        {
            _library.Run("Test1", 2);
            _library.Run("Test1", 2);
        }
    }


}
