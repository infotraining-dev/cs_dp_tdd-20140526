﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace MoqMocks
{
    [TestClass]
    public class TestsWithMoq
    {
        private ILongRunningLibrary _longRunningLibrary;

        [TestInitialize]
        public void SetupTest()
        {
            var moqLongRunningLibrary = new Mock<ILongRunningLibrary>(MockBehavior.Strict);

            moqLongRunningLibrary
                .Setup(m => m.RunForALongTime(It.IsInRange<int>(1, 12, Range.Inclusive)))
                .Returns("Range - [1, 12]");

            moqLongRunningLibrary
               .Setup(lrl => lrl.RunForALongTime(13))
               .Returns( (int s) => string.Format("This method has been mocked. The input value was {0}.", s));

            moqLongRunningLibrary.Setup(m => m.RunForALongTime(It.Is((int i) => i > 13)))
                .Returns((int arg) => string.Format("{0} >13", arg));
       
            moqLongRunningLibrary
                .Setup(lrl => lrl.RunForALongTime(0))
                .Throws(new ArgumentException("0 is not a valid parameter."));

            _longRunningLibrary = moqLongRunningLibrary.Object;
        }

        [TestMethod]
        public void TestLongRunningLibrary()
        {
            const int interval = 123;
            var result = _longRunningLibrary.RunForALongTime(interval);
            Debug.WriteLine(string.Format("Result from method was: {0}", result));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ShouldThrowWhenArgumentEquals0()
        {
            const int interval = 0;
            _longRunningLibrary.RunForALongTime(interval);
        }

        [TestMethod]
        public void SholdCallRunOnLibrary()
        {
            // Arrange
            Mock<ILongRunningLibrary> _mqLibrary = new Mock<ILongRunningLibrary>();
            DependentObject _sut = new DependentObject(_mqLibrary.Object);

            // Act
            _sut.DoStuff();

            // Assert
            _mqLibrary.Verify(m => m.Run("Test1", It.IsAny<int>()), Times.Exactly(2));
        }
    }
}
