﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Decorator.TheoryCode
{
    class MainApp
    {
        /// <summary>
        /// Decorator Design Pattern
        /// </summary>
        static void Main()
        {
            // Create ConcreteComponent and two Decorators
            ConcreteComponent c = new ConcreteComponent();
            ConcreteDecoratorA d1 = new ConcreteDecoratorA();
            ConcreteDecoratorB d2 = new ConcreteDecoratorB(new ConcreteDecoratorA(new ConcreteComponent()));

            // Link decorators
            d1.SetComponent(c);
            d2.SetComponent(d1);

            d2.Operation();

            Console.WriteLine("\n");

            // Mixed decoration
            Component mixed = new ConcreteDecoratorA(new ConcreteDecoratorB(new ConcreteComponent()));
            Console.WriteLine("mixed decoration -----\n");
            mixed.Operation();

            Console.WriteLine();
        }
    }

    // "Component"
    abstract class Component
    {
        public abstract void Operation();
    }

    // "ConcreteComponent"
    class ConcreteComponent : Component
    {
        public override void Operation()
        {
            Console.Write("ConcreteComponent ");
        }
    }

    // "Decorator"
    abstract class Decorator : Component
    {
        protected Component component;

        public Decorator()
        {
        }

        public Decorator(Component component)
        {
            this.component = component;
        }
        
        public void SetComponent(Component component)
        {
            this.component = component;
        }

        public override void Operation()
        {
            if (component != null)
            {
                component.Operation();
            }
        }
    }

    // "ConcreteDecoratorA" 
    class ConcreteDecoratorA : Decorator
    {
        private string addedState;

        public ConcreteDecoratorA()
        {
        }

        public ConcreteDecoratorA(Component component) : base(component)
        {
        }

        public override void Operation()
        {
            base.Operation();
            addedState = "decorated with New State ";
            Console.Write(addedState);
        }
    }

    // "ConcreteDecoratorB"
    class ConcreteDecoratorB : Decorator
    {
        public ConcreteDecoratorB()
        {
        }

        public ConcreteDecoratorB(Component component) : base(component)
        {
        }

        public override void Operation()
        {
            base.Operation();
            Console.Write(AddedBehavior());
        }

        string AddedBehavior()
        {
            return "decorated with AddedBehaviour() ";
        }
    }
}
