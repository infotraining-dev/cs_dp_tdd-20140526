﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.State.Example
{
    interface ITurnstileState
    {
        ITurnstileState Coin(TurnstileAPI t);
        ITurnstileState Pass(TurnstileAPI t);
    }

    class TurstileLocked : ITurnstileState
    {
        #region ITurnstilleState Members

        public ITurnstileState Coin(TurnstileAPI t)
        {
            t.Unlock();

            return new TurstileUnlocked();
        }

        public ITurnstileState Pass(TurnstileAPI t)
        {
            t.Alarm();

            return this;
        }

        #endregion
    }

    class TurstileUnlocked : ITurnstileState
    {
        #region ITurnstilleState Members

        public ITurnstileState Coin(TurnstileAPI t)
        {
            t.ThankYou();

            return this;
        }

        public ITurnstileState Pass(TurnstileAPI t)
        {
            t.Lock();
            return new TurstileLocked();
        }

        #endregion
    }

    public class TurnstileAPI
    {
        public void Lock()
        {
            Console.WriteLine("LOCKED");
        }

        public void Unlock()
        {
            Console.WriteLine("UNLOCKED");
        }

        public void Alarm()
        {
            Console.WriteLine("ALARM");
        }

        public void ThankYou()
        {
            Console.WriteLine("THANK YOU");
        }
    }

    class Turnstile
    {
        ITurnstileState state;
        TurnstileAPI api = new TurnstileAPI();

        public Turnstile(ITurnstileState ts)
        {
            state = ts;
        }

        public void Coin()
        {
            state = state.Coin(api);
        }

        public void Pass()
        {
            state = state.Pass(api);
        }
    }

    class Program
    {
        public static void Main()
        {
            Turnstile t = new Turnstile(new TurstileLocked());

            t.Coin();
            t.Pass();
            t.Pass();
            t.Coin();
            t.Coin();
            t.Pass();
            t.Pass();
        }
    }
}
