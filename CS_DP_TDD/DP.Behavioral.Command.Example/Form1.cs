﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DP.Behavioral.Command.Example
{
    public partial class FormMain : Form
    {
        ICommand cmdCopy;
        ICommand cmdPaste;
        ICommand cmdToUpper;
        ICommand cmdToLower;
        ICommand cmdRemove;
        ICommand cmdUndo;
        ICommandTracker _commandTracker = new CommandTracker();

        public FormMain()
        {
            InitializeComponent();

            cmdCopy = new CopyCommand(txtDocument);
            cmdPaste = new PasteCommand(txtDocument, _commandTracker);
            cmdToUpper = new ToUpperCommand(txtDocument, _commandTracker);
            cmdToLower = new ToLowerCommand(txtDocument, _commandTracker);
            cmdRemove = new RemoveCommand(txtDocument, _commandTracker);
            cmdUndo = new UndoCommand(_commandTracker);
        }

        private void btnToUpper_Click(object sender, EventArgs e)
        {
            cmdToUpper.Execute();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {     
            cmdCopy.Execute();
        }

        private void btnPaste_Click(object sender, EventArgs e)
        {            
            cmdPaste.Execute();
        }

        private void txtDocument_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            cmdRemove.Execute();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            try
            {
                cmdUndo.Execute();
            }
            catch (Exception excpt)
            {
                MessageBox.Show("Empty undo list...");
            }
        }

        private void btnToLower_Click(object sender, EventArgs e)
        {
            cmdToLower.Execute();
        }
    }
}
