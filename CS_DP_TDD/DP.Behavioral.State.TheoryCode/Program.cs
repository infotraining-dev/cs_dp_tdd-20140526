﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.State.TheoryCode
{
    /// <summary>
    /// State Design Pattern.
    /// </summary>
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            // Setup context in a state
            Context c = new Context();

            // Issue requests, which toggles state
            c.Request();
            c.Request();
            c.Request();
            c.Request();

            // Wait for user
            Console.Read();
        }
    }

    // "State" 
    abstract class State
    {
        public abstract State Handle(Context context);
    }

    // "ConcreteStateA"
    class ConcreteStateA : State
    {
        public override State Handle(Context context)
        {
            return new ConcreteStateB();
        }
    }

    // "ConcreteStateB" 
    class ConcreteStateB : State
    {
        public override State Handle(Context context)
        {
            return new ConcreteStateA();
        }
    }

    // "Context" 
    class Context
    {
        private State state;

        // Constructor
        public Context()
        {
            this.State = new ConcreteStateA();
        }

        // Property
        public State State
        {
            get { return state; }
            
            private set
            {
                state = value;
                Console.WriteLine("State: " +
                    state.GetType().Name);
            }
        }

        public void Request()
        {
            State = state.Handle(this);
        }
    }
}
