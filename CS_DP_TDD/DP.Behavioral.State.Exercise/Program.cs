﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.State.Exercise
{
    /// <summary>
    /// State Design Pattern.
    /// </summary>

    enum State { SOLD_OUT, NO_QUARTER, HAS_QUARTER, SOLD };

    class GumballMachine
    {
        State state = State.SOLD_OUT;
        int count = 0;

        public GumballMachine(int count)
        {
            this.count = count;

            if (this.count > 0)
                state = State.NO_QUARTER;
        }

        public void InsertQuarter()
        {
            if (state == State.HAS_QUARTER)
            {
                Console.WriteLine("You can't insert another quarter");
            }
            else if (state == State.NO_QUARTER)
            {
                state = State.HAS_QUARTER;
                Console.WriteLine("You inserted a quarter");
            }
            else if (state == State.SOLD_OUT)
            {
                Console.WriteLine("You can't insert a quarter, the machine is sold out");
            }
            else if (state == State.SOLD)
            {
                Console.WriteLine("Please wait, we're already giving you a gumball");
            }
        }

        public void EjectQuarter()
        {
            if (state == State.HAS_QUARTER)
            {
                Console.WriteLine("Quarter returned");
                state = State.NO_QUARTER;
            }
            else if (state == State.NO_QUARTER)
            {
                Console.WriteLine("You haven't inserted a quarter");
            }
            else if (state == State.SOLD)
            {
                Console.WriteLine("Sorry, you already turned the crank");
            }
            else if (state == State.SOLD_OUT)
            {
                Console.WriteLine("You can't eject, you haven't inserted a quarter yet");
            }
        }

        public void TurnCrank()
        {
            if (state == State.SOLD)
            {
                Console.WriteLine("Turning twice doesn't get you another gumball!");
            }
            else if (state == State.NO_QUARTER)
            {
                Console.WriteLine("You turned but there's no quarter");
            }
            else if (state == State.SOLD_OUT)
            {
                Console.WriteLine("You turned, but there are no gumballs");
            }
            else if (state == State.HAS_QUARTER)
            {
                Console.WriteLine("You turned...");
                state = State.SOLD;
                Dispense();
            }
        }

        public void Dispense()
        {
            if (state == State.SOLD)
            {
                Console.WriteLine("A gumball comes rolling out the slot");
                count = count - 1;
                if (count == 0)
                {
                    Console.WriteLine("Oops, out of gumballs!");
                    state = State.SOLD_OUT;
                }
                else
                {
                    state = State.NO_QUARTER;
                }
            }
            else if (state == State.NO_QUARTER)
            {
                Console.WriteLine("You need to pay first");
            }
            else if (state == State.SOLD_OUT)
            {
                Console.WriteLine("No gumball dispensed");
            }
            else if (state == State.HAS_QUARTER)
            {
                Console.WriteLine("No gumball dispensed");
            }
        }

        public void Refill(int numGumBalls)
        {
            this.count = numGumBalls;
            state = State.NO_QUARTER;
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.Append("\nMighty Gumball, Inc.");
            result.Append("\n.NET Standing Gumball Model #2010\n");
            result.Append("Inventory: " + count + " gumball");
            if (count != 1)
            {
                result.Append("s");
            }
            result.Append("\nMachine is ");
            if (state == State.SOLD_OUT)
            {
                result.Append("sold out");
            }
            else if (state == State.NO_QUARTER)
            {
                result.Append("waiting for quarter");
            }
            else if (state == State.HAS_QUARTER)
            {
                result.Append("waiting for turn of crank");
            }
            else if (state == State.SOLD)
            {
                result.Append("delivering a gumball");
            }
            result.Append("\n");
            return result.ToString();
        }
    }

    class Program
    {
        public static void Main()
        {
            GumballMachine gumballMachine = new GumballMachine(5);

		    Console.WriteLine(gumballMachine);

		    gumballMachine.InsertQuarter();
		    gumballMachine.TurnCrank();

		    Console.WriteLine(gumballMachine);

		    gumballMachine.InsertQuarter();
		    gumballMachine.EjectQuarter();
		    gumballMachine.TurnCrank();

		    Console.WriteLine(gumballMachine);

		    gumballMachine.InsertQuarter();
		    gumballMachine.TurnCrank();
		    gumballMachine.InsertQuarter();
		    gumballMachine.TurnCrank();
		    gumballMachine.EjectQuarter();

		    Console.WriteLine(gumballMachine);

		    gumballMachine.InsertQuarter();
		    gumballMachine.InsertQuarter();
		    gumballMachine.TurnCrank();
		    gumballMachine.InsertQuarter();
		    gumballMachine.TurnCrank();
		    gumballMachine.InsertQuarter();
		    gumballMachine.TurnCrank();

		    Console.WriteLine(gumballMachine); 
        }
    }
}
