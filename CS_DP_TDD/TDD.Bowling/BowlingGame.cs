﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDD.Bowling
{
    public class BowlingGame
    {
        private int[] _rolls = new int[21];
        private int _currentRoll;
        private int _score;
        private const int MAX_NUMBER_OF_FRAMES = 10;
        private const int MAX_POINTS_IN_FRAME = 10;

        public int Score
        {
            get
            {
                int frameIndex = 0;
                for (int frame = 0; frame < MAX_NUMBER_OF_FRAMES; ++frame)
                {
                    if (IsStrike(frameIndex))
                    {
                        _score += MAX_POINTS_IN_FRAME + StrikeBonus(frameIndex);
                        frameIndex++;
                    }
                    else
                    {
                        _score += SumOfPinsInFrame(frameIndex);

                        if (IsSpare(frameIndex))
                        {
                            _score += SpareBonus(frameIndex);
                        }

                        frameIndex += 2;
                    }
                }
                return _score;
            }
        }

        private int StrikeBonus(int frameIndex)
        {
            return _rolls[frameIndex + 1] + _rolls[frameIndex + 2];
        }

        private bool IsStrike(int frameIndex)
        {
            return _rolls[frameIndex] == MAX_POINTS_IN_FRAME;
        }

        private bool IsSpare(int frame)
        {
            return SumOfPinsInFrame(frame) == MAX_POINTS_IN_FRAME;
        }

        private int SpareBonus(int frameIndex)
        {
            return _rolls[frameIndex + 2];
        }

        private int SumOfPinsInFrame(int frameIndex)
        {
            return _rolls[frameIndex] + _rolls[frameIndex + 1];
        }

        public void Roll(int pins)
        {
            _rolls[_currentRoll++] = pins;
        }
    }
}
