﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;
using TDD.RecentlyUsedList_Tests.RecentlyUsedListImpl;

namespace TDD.RecentlyUsedList_Tests.Specs
{
    [Subject(typeof(RecentlyUsedList))]
    public class With_RecentlyUsedList
    {
        private Establish context = () => _recentlyUsedList = new RecentlyUsedList();

        protected static RecentlyUsedList _recentlyUsedList;
    }

    [Subject(typeof(RecentlyUsedList))]
    public class When_using_a_new_created_list : With_RecentlyUsedList
    {
        private Because of = () => _recentlyUsedList.Add("item");

        private It should_resize_after_inserting_an_item
            = () => _recentlyUsedList.Count.ShouldEqual(1);

        private It should_store_inserted_item_at_index_zero 
            = () => _recentlyUsedList[0].ShouldEqual("item");
    }

    [Subject(typeof(RecentlyUsedList), "Populated")]
    public class When_using_a_populated_list : With_RecentlyUsedList
    {
        private Establish context = () =>
        {
            _recentlyUsedList.Add("item1");
            _recentlyUsedList.Add("item2");
        };

        Because of = () => { _recentlyUsedList.Add("item3"); };

        private It should_store_last_inserted_item_at_index_zero = () => _recentlyUsedList[0].ShouldEqual("item3");
    }
}

