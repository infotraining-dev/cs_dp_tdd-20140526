﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TDD.Bowling;

namespace TDD.Bowling_Tests
{

    

    [TestFixture]
    public class BowlingGameTests
    {
        private BowlingGame _game;

        [SetUp]
        public void StartBowlingGame()
        {
            _game = new BowlingGame();
        }
        private void RollMany(int count, int pins)
        {
            for (int i = 0; i < count; ++i)
                _game.Roll(pins);
        }

        [Test]
        public void WhenGameStartsScoreShouldBeZero()
        {
            Assert.That(_game.Score, Is.EqualTo(0));
        }

        [Test]
        public void WhenAllThrowsInGutterScoreShouldBeZero()
        {
            RollMany(20, 0);

            Assert.That(_game.Score, Is.EqualTo(0));
        }

        [TestCase(1, 20)]
        [TestCase(2, 40)]
        [TestCase(4, 80)]
        public void WhenAllRollsNoMarkScoreShouldBeSumOfPins(int pins, int expectedScore)
        {
            RollMany(20, pins);

            Assert.That(_game.Score, Is.EqualTo(expectedScore));
        }

        [Test]
        public void WhenSpareNextRollIsCountedTwice()
        {
            // Arrange

            // Act
            RollSpare();
            _game.Roll(3);
            RollMany(17, 0);

            // Assert
            Assert.That(_game.Score, Is.EqualTo(16));
        }

        [Test]
        public void WhenStrikeNextTwoRollsAreCountedTwice()
        {
            RollStrike();
            _game.Roll(2);
            _game.Roll(2);
            RollMany(16, 0);

            Assert.That(_game.Score, Is.EqualTo(18));
        }

        [Test]
        public void WhenPerfectGameScoreShouldBe300()
        {
            for(int i = 0; i < 12; ++i)
                RollStrike();

            Assert.That(_game.Score, Is.EqualTo(300));
        }

        private void RollStrike()
        {
            _game.Roll(10);
        }

        private void RollSpare()
        {
            _game.Roll(5);
            _game.Roll(5); 
        }
    }
}
