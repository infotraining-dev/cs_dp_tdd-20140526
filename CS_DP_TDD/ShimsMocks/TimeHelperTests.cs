using System;
using Microsoft.QualityTools.Testing.Fakes;
using NUnit.Framework;

namespace ShimsMocks
{
    [TestFixture]
    public class TimeHelperTests
    {
        [Test]
        public void IsFutureDateShouldWorkInEveryCase()
        {
            DateTime date = new DateTime(2014, 6, 1);

            using (ShimsContext.Create())
            {
                DateTime shimDate = new DateTime(2014, 1, 1);

                System.Fakes.ShimDateTime.NowGet = () => shimDate;

                Assert.True(TimeHelper.IsFutureDate(date));    
            }
        }
    }
}