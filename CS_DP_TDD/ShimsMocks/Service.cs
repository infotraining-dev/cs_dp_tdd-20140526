﻿using System.Collections.Generic;
using Microsoft.QualityTools.Testing.Fakes;
using NUnit.Framework;
using System;
using System.Web;

namespace ShimsMocks
{
    public class Service
    {
        public void StoreInSession(string key, string item)
        {
            HttpContext.Current.Session[key] = item;
        }

        public string LoadFromSession(string key)
        {
            string item = HttpContext.Current.Session[key] as string;

            if (item == null)
                throw new ApplicationException("Session empty");
            
            return item;
        }
    }

    [TestFixture]
    public class ServiceTests
    {
        [Test]
        public void CanStoreItemInSession()
        {
            // Arrange
            Dictionary<string, object> shimSession = new Dictionary<string, object>();

            using (ShimsContext.Create())
            {
                var session = new System.Web.SessionState.Fakes.ShimHttpSessionState();
                session.ItemSetStringObject = (k, v) => shimSession.Add(k, v);
                 
                var context = new System.Web.Fakes.ShimHttpContext();
                context.SessionGet = () => session;

                System.Web.Fakes.ShimHttpContext.CurrentGet = () => context;

                string key = "key";
                string item = "item";

                Service sut = new Service();

                // Act
                sut.StoreInSession(key, item);

                // Assert
                Assert.That(shimSession[key], Is.EqualTo(item));
            }
        }
    }
}