﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShimsMocks
{
    public static class TimeHelper
    {
        public static bool IsFutureDate(DateTime d)
        {
            return DateTime.Now < d;
        }
    }
}
