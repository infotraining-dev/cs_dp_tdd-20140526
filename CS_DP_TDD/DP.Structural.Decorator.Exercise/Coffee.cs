﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Decorator.Exercise
{
    /// <summary>
    ///  Coffee
    /// </summary>
    public abstract class Coffee
    {
        protected double price;
        protected string description;

        public virtual double GetTotalPrice()
        {
            return price;
        }

        public virtual string GetDescription()
        {
            return description;
        }

        public abstract void Prepare();
    }

    /// <summary>
    /// Espresso
    /// </summary>
    public class Espresso : Coffee
    {
        public Espresso()
        {
            price = 4.0;
            description = "Espresso";
        }

        public override void Prepare()
        {
            Console.WriteLine("Making a perfect Espresso: 7g, 15bar, 95degC & 25sec");
        }
    }

    /// <summary>
    /// Cappuccino
    /// </summary>
    public class Cappuccino : Coffee
    {
        public Cappuccino()
        {
            price = 6.0;
            description = "Cappuccino";
        }

        public override void Prepare()
        {
            Console.WriteLine("Making a perfect Cappuccino");
        }
    }

    /// <summary>
    /// Latte
    /// </summary>
    public class Latte : Coffee
    {
        public Latte()
        {
            price = 8.0;
            description = "Latte";
        }

        public override void Prepare()
        {
            Console.WriteLine("Making a superb Latte");
        }
    }

    // TO DO: Dodatki: cena - Whipped: 2.5, Whiskey: 6.0, ExtraEspresso: 4.0

    // TO DO: Zastapic statyczne klasy EspressoConPanna, CaffeLatteExtraEspresso, IrishCream  

    class CoffeeDecorator : Coffee
    {
        protected Coffee _coffee;

        public Coffee Coffee
        {
            get { return _coffee; }
            set { _coffee = value; }
        }

        public CoffeeDecorator()
        {
        }

        public CoffeeDecorator(Coffee coffee, double price, string description)
        {
            _coffee = coffee;
            this.price = price;
            this.description = description;
        }

        public override double GetTotalPrice()
        {
            return price + _coffee.GetTotalPrice();
        }

        public override string GetDescription()
        {
            return _coffee.GetDescription() + " " + description;
        }

        public override void Prepare()
        {
            _coffee.Prepare();
        }
    }

    class Whipped : CoffeeDecorator
    {
        public Whipped(Coffee coffee) : base(coffee, 2.5, "Whipped")
        {
        }

        public Whipped() : base(null, 2.5, "Whipped")
        {
        }

        public override void Prepare()
        {
            base.Prepare();
            Console.WriteLine("Adding a whipped cream...");
        }
    }

    class Whisky : CoffeeDecorator
    {
        public Whisky(Coffee coffee)
            : base(coffee, 6.0, "Whisky")
        {
        }

        public Whisky() : base(null, 6.0, "Whisky")
        {}

        public override void Prepare()
        {
            base.Prepare();
            Console.WriteLine("Pouring 50 cl of single malt...");
        }
    }

    class ExtraEspresso : CoffeeDecorator
    {
        public ExtraEspresso(Coffee coffee)
            : base(coffee, 4.0, "Extra espresso")
        {
        }

        public override void Prepare()
        {
            base.Prepare();
            Console.WriteLine("And espresso once more...");
        }
    }
}
