﻿using NUnit.Framework;

namespace StubsVsMocks
{
    [TestFixture]
    public class OrderStateTests
    {
        private const string TALISKER = "Talisker";
        private const string HIGHLAND_PARK = "Highland Park";
        private IWarehouse _warehouse;

        [SetUp]
        public void InitTest()
        {
            _warehouse = new WarehouseImpl();
            _warehouse.Add(TALISKER, 50);
            _warehouse.Add(HIGHLAND_PARK, 25);
        }

        [Test]
        public void TestOrderIsFilledIfEnoughInWarehouse()
        {
            // Arrange
            Order order = new Order(TALISKER, 50);
            
            // Act
            order.Fill(_warehouse);
            
            // Assert
            Assert.IsTrue(order.IsFilled);
            Assert.AreEqual(0, _warehouse.GetInventory(TALISKER));
        }

        [Test]
        public void TestOrderDoesNotRemoveIfNotEnough()
        {
            Order order = new Order(TALISKER, 51);
            
            order.Fill(_warehouse);
            
            Assert.IsFalse(order.IsFilled);
            Assert.AreEqual(50, _warehouse.GetInventory(TALISKER));
        }
    }
}