﻿Feature: ExchangeRate
	In order to be a rich
	As a future currency speculator
	I want to know an exchange rate for given currency

@HappyPath
Scenario Outline: Get an exchange rate for currency
	Given I have entered <CurrencyCode> as a currency code 
	And I have entered <Date> as a date
	When I ask for exchange rate
	Then the <Rate> and <CFactor> should be returned

	Examples: 
		| CurrencyCode  | Date           | Rate   | CFactor |
		| USD           | 2014/05/06     | 3.0196 | 1       |
		| INR           | 2014/05/06     | 5.0201 | 100     |
		| USD           | 2014/05/29     | 3.0395 | 1       |


#@SadPath
#Scenario: Rates in weekends
#	Given I have entered "USD" as a currency code 
#	And I have entered "2014/05/25" as a date
#	When I ask for exchange rate
#	Then the rate "3.0490" from the last valid date should be returned 
#
