﻿using System;
using System.Globalization;
using FluentAssertions;
using NBP.CurrencyService;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace NBP.CurrencyAnalyzer.Acceptance_Tests.Steps
{
    [Binding]
    public class ExchangeRateSteps
    {
        private string _currencyCode;
        private DateTime _date;
        private ExchangeRate _result;
        private CurrencyExchangeService _service;

        [BeforeScenario]
        public void InitService()
        {
            _service = new CurrencyExchangeService(new FileProvider("Data"), new RateParser());
        }

        [Given]
        public void Given_I_have_entered_CURRENCYCODE_as_a_currency_code(string currencyCode)
        {
            _currencyCode = currencyCode;
        }
        
        [Given]
        public void Given_I_have_entered_DATE_as_a_date(string date)
        {
            _date = DateTime.ParseExact(date, "yyyy/MM/dd", CultureInfo.InvariantCulture);
        }
        
        [When]
        public void When_I_ask_for_exchange_rate()
        {
            _result = _service.GetExchangeRate(_currencyCode, _date);
        }
        
        [Then]
        public void Then_the_RATE_and_CFACTOR_should_be_returned(decimal rate, int cFactor)
        {
            _result.CFactor.Should().Equals(cFactor);
            Assert.AreEqual(_result.Rate, rate);            
        }
    }
}
