﻿namespace NBP.CurrencyService
{
    public class ExchangeRate
    {
        public decimal Rate { get; set; }
        public int CFactor { get; set; }
    }
}
