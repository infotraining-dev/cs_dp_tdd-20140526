﻿using System.IO;

namespace NBP.CurrencyService
{
    public class FileProvider : IFileProvider
    {
        private string directoryPath;

        public FileProvider(string directoryPath)
        {
            this.directoryPath = directoryPath;
        }

        public string LoadFile(string fileName)
        {
            string path = Path.Combine(directoryPath, fileName);
            string content;

            using (TextReader reader = new StreamReader(path))
            {
                content = reader.ReadToEnd();
            }

            return content;
        }
    }
}