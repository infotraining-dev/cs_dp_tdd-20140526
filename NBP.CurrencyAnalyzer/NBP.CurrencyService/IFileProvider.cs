namespace NBP.CurrencyService
{
    public interface IFileProvider
    {
        string LoadFile(string s);
    }
}