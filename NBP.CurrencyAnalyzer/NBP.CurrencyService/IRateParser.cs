﻿namespace NBP.CurrencyService
{
    public interface IRateParser
    {
        ExchangeRate Parse(string content, string currencyCode);
    }
}