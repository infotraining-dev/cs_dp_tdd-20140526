using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace NBP.CurrencyService
{
    public class RateParser : IRateParser
    {
        public ExchangeRate Parse(string content, string currencyCode)
        {
            XElement doc = XElement.Parse(content);

            foreach(var e in doc.Elements())
                Debug.Write(e.Name);

            var currencyCodeElement = doc.Descendants("kod_waluty").FirstOrDefault(e => e.Value == currencyCode);
            var rate = decimal.Parse(currencyCodeElement.ElementsAfterSelf("kurs_sredni").FirstOrDefault().Value, CultureInfo.GetCultureInfo("pl-PL"));

            var cfactor = int.Parse(currencyCodeElement.ElementsBeforeSelf("przelicznik").FirstOrDefault().Value);

            return new ExchangeRate() { CFactor = cfactor, Rate = rate };   
        }
    }
}