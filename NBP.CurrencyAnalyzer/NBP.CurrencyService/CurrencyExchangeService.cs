﻿using System;
using System.IO;
using System.Linq;

namespace NBP.CurrencyService
{
    public class CurrencyExchangeService
    {
        private readonly IFileProvider _fileProvider;
        private readonly IRateParser _rateParser;

        public CurrencyExchangeService(IFileProvider fileProvider, IRateParser rateParser)
        {
            _fileProvider = fileProvider;
            _rateParser = rateParser;
        }

        public ExchangeRate GetExchangeRate(string currencyCode, DateTime date)
        {
            string dirContent = _fileProvider.LoadFile("dir.txt");

            var fileNames = dirContent.Split(new char[] {'\n', '\r'}, StringSplitOptions.RemoveEmptyEntries);
            
            string formatedDate = string.Format("{0,2:00}{1,2:00}{2,2:00}", date.Year.ToString().Substring(2), date.Month, date.Day);

            var rateFileName = fileNames.Where(fn => fn.StartsWith("a", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault(fn => fn.EndsWith(formatedDate));

            string content = _fileProvider.LoadFile(rateFileName + ".xml");

            var exchangeRate = _rateParser.Parse(content, currencyCode);

            return exchangeRate;
        }
    }
}
