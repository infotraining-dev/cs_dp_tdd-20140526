﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DataDownloader
{
    /* http://www.nbp.pl/home.aspx?f=/kursy/instrukcja_pobierania_kursow_walut.html */

    public class CurrencyExchangeRateDownloader
    {
        private const string dataDir = "NBP_Data";
        private const string dirFileName = "dir.txt";
        private const string xmlFileFormat = "{0}.xml";
        private const string urlFormat = "http://www.nbp.pl/kursy/xml/" + xmlFileFormat;

        public void DownloadExchangeRates(int nrOfRates = 100)
        {
            DownloadFileList();

            List<string> tableNames = ParseDirFile();

            foreach (var tableName in tableNames.Skip(tableNames.Count - nrOfRates))
            {
                DownloadCurrencyExchangeRate(tableName);
            }
        }

        private List<string> ParseDirFile()
        {
            string content = File.ReadAllText(Path.Combine(dataDir, dirFileName));

            return content.Split(new char[] {'\n', '\r'}, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        private void DownloadFileList()
        {
            Console.WriteLine("Downloading dir.txt...");

            WebClient client = new WebClient();

            string fileList = client.DownloadString("http://www.nbp.pl/kursy/xml/dir.txt ");

            if (!Directory.Exists(dataDir))
            {
                Directory.CreateDirectory(dataDir);
            }

            using (var stream = File.CreateText(Path.Combine(dataDir, dirFileName)))
            {
                stream.Write(fileList);
            }

            Console.WriteLine("dir.txt saved.");
        }

        private void DownloadCurrencyExchangeRate(string tableName)
        {
            Console.Write("Downloading " + tableName + "... ");

            WebClient client = new WebClient();
            
            var url = string.Format(urlFormat, tableName);
            var content = client.DownloadString(url);

            using (var stream = File.CreateText(Path.Combine(dataDir, string.Format(xmlFileFormat, tableName))))
            {
                stream.Write(content);
            }

            Console.WriteLine("Done.");
        }

        public static void Main()
        {
            new CurrencyExchangeRateDownloader().DownloadExchangeRates();
        }
    }
}
